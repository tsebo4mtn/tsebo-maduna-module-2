void main() {
  var winnersList = [
    'Health ID',
    'Trans Union Dealer Guide',
    'Rapid Targets',
    'Matchy',
    'Plascon InspireMe',
    'phrazapp',
    'Dstv',
    'Comm Telco Data Visualizer',
    'Price Check Mobile',
    'Market Share',
    'Nedbank App Suite',
    'Snap Scan',
    'Kidsaid',
    'Bookly',
    'Gautrain Buddy',
    'Supersport',
    'Sync Mobile',
    'My Belongings',
    'Live Inspect',
    'Virgo',
    'Zapper',
    'Rea Vaya',
    'Wildlife Tracker',
    'Wumdrop',
    'Dstvnow',
    'Vula Mobile',
    'Cput Mobile',
    'M4jam',
    'Domestly',
    'Ikhokha',
    'Hearza',
    'Tutorme',
    'Kaching',
    'Friendly Math Monsters',
    'Orderin',
    'Pick N Pay',
    'Transunion Check 1',
    'Hey Jude',
    'Our Social',
    'Awethu Project',
    'Zulzi',
    'Shyst',
    'Eco Slips',
    'Cowa Bunga',
    'Pineapple',
    'Khula Ecosystem',
    'Besmarta.Com',
    'Ctrl.',
    'Stockfella',
    'Asi Snakes',
    'Digger',
    'Matric Live',
    'Franc',
    'Naked Insurance',
    'Mowash',
    'Over',
    'Vula',
    'Hydra',
    'My Pregnancy Journey',
    'Loctransie',
    'Checkers Sicxty60',
    'Stockfella',
    'Bottles',
    'Green Fingers Mobile',
    'My Pregnancy Journey',
    'Guardian Health',
    'Matric Live',
    'Easy Equities',
    'Technishen',
    'Xitsonga',
    'Birdpro',
    'Lexie Hearing App',
    'Examsta',
    'Takealot',
    'Shyft',
    'Sisa',
    'Murimi',
    'Rekindle',
    'Hellopay',
    'Kazinow',
    'Uniwise',
    'Guardian Health',
    'Ambani Africa',
  ];

  var appsWon = [];
  for (final appName in winnersList) {
    var app = App(appName, 'Farming', 'Tsebo', '2012');
    appsWon.add(app);
    //// print(app.theName +' '+ app.categoryOrSector +' ' + app.theDeveloper + ' ' + app.theYearItWon);
  }
  appsWon.add(App("Lesego", "Tsebo", "Test", "2021"));
  appsWon.add(App("Life", "Owner", "Dev", "2020"));
  var app = App('Whatever', 'Farming', 'Tsebo', '2012');
  print(app);
  for (final app in appsWon) {
    print(app.theName +
        ' ' +
        app.categoryOrSector +
        ' ' +
        app.theDeveloper +
        ' ' +
        app.theYearItWon);
  }
}

class App {
  String theName = 'Khula';
  String categoryOrSector = 'Farming';
  String theDeveloper = 'Tsebo';
  String theYearItWon = '2021';

  App(String theName, String categoryOrSector, String theDeveloper,
      String theYearItWon) {
    this.theName = theName;
    this.categoryOrSector = categoryOrSector;
    this.theDeveloper = theDeveloper;
    this.theYearItWon = theYearItWon;
  }
}
